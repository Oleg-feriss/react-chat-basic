const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
  addMessage(messageData) {
    const newMessage = MessageRepository.create(messageData);
    if (!message) {
      throw "Message can't be created";
    }
    return newMessage;
  }

  search(search) {
    const message = MessageRepository.getOne(search);
    if (!message) {
      throw 'Massage not found';
    }
    return message;
  }

  getMessages() {
    const messages = MessageRepository.getAll();
    if (!messages) {
      throw 'Messages not found';
    }
    return messages;
  }

  getMessage(id) {
    const message = this.search(id);
    if (!message) {
      return null;
    }
    return message;
  }

  removeMessage(id) {
    const removed = MessageRepository.delete(id);
    if (!removed) {
      throw "Message can't be removed";
    }
    return removed;
  }

  updateMessage(id, data) {
    const updated = MessageRepository.update(id, data);
    if (!updated) {
      throw "Message can't be updated";
    }
    return updated;
  }
}

module.exports = new MessageService();
