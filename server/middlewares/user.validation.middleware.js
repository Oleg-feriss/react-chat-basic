const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
  const { id, ...rest } = req.body;

  if (id || !isModelComplete(rest) || !isBodyValid(rest)) {
    res.status(400);
    res.err = 'User entity to create is not valid';
  }

  next();
};

const updateUserValid = (req, res, next) => {
  const { id, ...rest } = req.body;

  if (id || !isBodyValid(rest) || isEmptyObject(rest)) {
    res.status(400);
    res.err = 'User entity to update is not valid';
  }

  next();
};

const isModelComplete = (data) => {
  for (const key in user) {
    if (key !== 'id' && !data[key]) {
      return false;
    }
  }
  return true;
};

const isEmptyObject = (data) =>
  Object.keys(data).length === 0 && data.constructor === Object;

const isBodyValid = (data) => {
  for (const key in data) {
    if (!isValid(key, data) || !isPrimitive(data[key])) {
      return false;
    }
  }
  return true;
};

const isValid = (keyType, data) => {
  switch (keyType) {
    case 'username':
      return data[keyType];
    case 'isAdmin':
      return data[keyType];
    case 'email':
      return isEmailValid(data[keyType]);
    case 'phoneNumber':
      return isPasswordValid(data[keyType]);
    default:
      return false;
  }
};

const isEmailValid = (email) =>
  email &&
  (typeof email === 'string' || email instanceof String) &&
  email.split('@')[1] === 'gmail.com' &&
  email.split('@').length === 2 &&
  isUnique(email);

const isUnique = (search) => {
  const users = UserService.getAllUsers(search);
  for (const user of users) {
    if (search === user.email || search === user.phoneNumber) {
      return false;
    }
  }
  return true;
};

const isPasswordValid = (password) => password.length > 2;

const isPrimitive = (value) => typeof value !== 'object';

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
