const userRoutes = require('./userRoutes');
const authRoutes = require('./loginRoutes');
const messageRoutes = require('./messageRoutes');

module.exports = (app) => {
  app.use('/api/users', userRoutes);
  app.use('/api/login', authRoutes);
  app.use('/api/messages', messageRoutes);
};
