const { Router } = require('express');
const { responseMiddleware } = require('../middlewares/response.middleware');
const messageService = require('../services/messageService');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    const messages = messageService.getMessages();

    if (!messages) {
      res.err = {
        status: 404,
        message: 'Failed to get messages',
      };
    } else {
      res.body = messages;
    }
    next();
  },
  responseMiddleware
);

router.post(
  '/',
  (req, res, next) => {
    const { body } = req;

    if (!body.avatar) {
      body.avatar =
        'https://upload.wikimedia.org/wikipedia/en/b/b1/Portrait_placeholder.png';
    }

    const message = messageService.addMessage(body);
    if (!message) {
      res.err = {
        status: 400,
        message: 'Failed to create',
      };
    } else {
      res.body = message;
    }
    next();
  },
  responseMiddleware
);

router.put(
  '/:id',
  (req, res, next) => {
    const { id } = req.params;
    const message = messageService.getMessage({ id });

    if (!message) {
      res.err = {
        status: 404,
        message: 'Message not found',
      };
    } else {
      const { body } = req;
      const updated = messageService.updateMessage(id, body);

      if (!updated) {
        res.err = {
          status: 400,
          message: 'Failed to update message',
        };
      } else {
        res.body = updated;
      }
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    const { id } = req.params;
    const removed = messageService.removeMessage(id);

    if (removed.length === 0) {
      res.err = {
        status: 400,
        message: 'Failed to remove',
      };
    } else {
      res.body = {};
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
