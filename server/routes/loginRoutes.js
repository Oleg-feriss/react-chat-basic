const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/',
  (req, res, next) => {
    const user = AuthService.login(req.body);
    if (!user) {
      res.err = {
        status: 404,
        message: 'User not found',
      };
    } else {
      res.body = user;
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
