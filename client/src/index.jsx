import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import store from './store/store';
import createRoutes from './routes';
import './index.css';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      {createRoutes}
    </Router>
  </Provider>,
  document.getElementById('root')
);