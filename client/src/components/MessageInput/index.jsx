import React, { useState } from 'react';

import './MessageInput.css';

const MessageInput = (props) => {
  const [input, setInput] = useState('');
  const newMessageHandler = (event) => {
    event.preventDefault();
    if (input.trim().length > 0) {
      props.setState([
        ...props.state,
        {
          id: Date.now(),
          userId: '533b5230-1b8f-11e8-9629-c7eca82ab7df',
          avatar:
            'https://mir-s3-cdn-cf.behance.net/project_modules/disp/ea7a3c32163929.567197ac70bda.png',
          createdAt: Date.now(),
          editedAt: '',
          text: input,
          user: 'Oleg',
        },
      ]);
      setInput('');
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      newMessageHandler(e);
    }
  };

  return (
    <div className="message-input">
      <input
        type="text"
        className="message-input-text"
        placeholder="new message"
        value={input}
        onChange={(e) => {
          setInput(e.target.value);
        }}
        onKeyDown={handleKeyPress}
      />
      <button onClick={newMessageHandler} className="message-input-button">
        SEND
      </button>
    </div>
  );
};

export default MessageInput;
