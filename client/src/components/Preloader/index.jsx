import React from 'react';

import logo from '../../img/logo.svg';
import './Preloader.css';

const Preloader = () => {
  return (
    <div className='center'>
      <img src={logo} className="load-logo" alt='logo'/>
    </div>
  );
};

export default Preloader;
