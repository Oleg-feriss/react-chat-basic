import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import Chat from './components/Chat';
import LoginPage from './containers/LoginPage';
import UserList from './containers/UserList';
import UserEditor from './containers/UserEditor';

export default (
  <Routes>
    <Route exact path="/">
      <Navigate to="/login"></Navigate>
    </Route>
    <Route path="/users" component={UserList} />
    <Route path="/usereditor" component={UserEditor} />
    <Route path="/login" component={LoginPage} />
    <Route exact path="/chat" component={Chat} />
  </Routes>
);
